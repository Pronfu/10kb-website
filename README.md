# 10kb-website
Creating a responsive website that is under 10kb in size. Based on 10k apart.

It is under 10kb,
![Page size and speed test](https://i.imgur.com/VuaBLik.png)

## Live Demo
[https://projects.gregoryhammond.ca/10kb-site/](https://projects.gregoryhammond.ca/10kb-site/)

## Faq
> Do I need a website to get this to work?

* No, you don't. You can run this locally on your computer, or have this just for practice (Find out how to do it thanks to [MakeTechEasier](https://www.maketecheasier.com/setup-local-web-server-all-platforms/) ), but if you wish to show your friends, family and coworkers what you have worked on then you will need to buy a domain and hosting.

## License
This project is under Unlicense or Beerware or WTFPL. You can use any of those licenses, credit is appreciated but not required.

## Thanks
Owen Versteeg for creating min framework - https://mincss.com/

10k Apart for creating this "challenge" - https://a-k-apart.com/
